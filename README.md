# Project Notes
*Updated March 3rd, 2019*

## Key Unit Configuration:
* Username/Display Name is located at line 44 `/cmd/talkiepi/main.go`
* Server is located on line 18 in `/cmd/talkiepi/main.go`

# Pi Setup

## GPIO

Pin assignments can be edited via `talkiepi.go`
```go
const (
	OnlineLEDPin       uint = 18
	ParticipantsLEDPin uint = 23
	TransmitLEDPin     uint = 24
	ButtonPin          uint = 25
)
```
Pinout Schematic can be found [here](https://gitlab.com/scottyedmonds/talkiepi/blob/742e8ba560066aae5b3908ca1d2164f6b510d58e/doc/Talkie-Pi%20GPIO%20Pinout.png)

# Installation

This document assumes that OS raspbian-stretch-lite is installed on the SD card, and that the distribution is up to date.
```
sudo apt-get update
sudo apt-get upgrade
```

This document also asumes that you have already configured network/wifi connectivity on your Raspberry Pi.

Create new file named `wpa_supplicant.conf` on `/boot/` partition of SD Card. Contents of file should be is using WPA-PSK:

```
country=CA
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
	ssid="WiFiNetworkSSID"
	psk="WiFiPassword"
	key_mgmt=WPA-PSK
}
```

By default talkiepi will run without any arguments, it will autogenerate a username and then connect to my mumble server.
You can change this behavior by appending commandline arguments of `-server YOUR_SERVER_ADDRESS`, `-username YOUR_USERNAME` to the ExecStart line in `/etc/systemd/system/mumble.service` once installed.

talkiepi will also accept arguments for `-password`, `-insecure`, `-certificate` and `-channel`, all defined in `cmd/talkiepi/main.go`, if you run your own mumble server, these will be self explanatory.


## Create a user

As root on your Raspberry Pi (`sudo -i`), create a mumble user:
```
adduser --disabled-password --disabled-login --gecos "" mumble
usermod -a -G cdrom,audio,video,plugdev,users,dialout,dip,input,gpio mumble
```

## Install

As root on device (`sudo -i`), install golang and other required dependencies, then build from source (*Note Pi Zero fix for libopenal below*):
```
apt-get install golang libopenal-dev libopus-dev git

su mumble

mkdir ~/gocode
mkdir ~/bin

export GOPATH=/home/mumble/gocode
export GOBIN=/home/mumble/bin

cd $GOPATH

go get github.com/dchote/gopus
go get gitlab.com/scottyedmonds/talkiepi

cd $GOPATH/src/gitlab.com/scottyedmonds/talkiepi

go build -o /home/mumble/bin/talkiepi cmd/talkiepi/main.go 
```


## Pi Zero Fixes
I have compiled libopenal without ARM NEON support so that it works on the Pi Zero. The packages can be found in the [workarounds](/workarounds/). directory of this repo, install the libopenal1 package over your existing libopenal install. (`*need to add CMD for this*`)

## Start on boot

As root on device (`sudo -i`), copy mumble.service in to place:
```
cp /home/mumble/gocode/src/gitlab.com/scottyedmonds/talkiepi/conf/systemd/mumble.service /etc/systemd/system/mumble.service

systemctl enable mumble.service
```

## Create a certificate
*Possible Alternative to VPN*

To register unit against the mumble server and apply ACLs.
```
su mumble
cd ~

openssl genrsa -aes256 -out key.pem
```

Enter a simple passphrase, we will remove it shortly...

```
openssl req -new -x509 -key key.pem -out cert.pem -days 1095
```

Enter passphrase again, and fill out the certificate info as much as you like, its not really that important if you're just hacking around with this.

```
openssl rsa -in key.pem -out nopasskey.pem
```

Enter password for the last time.

```
cat nopasskey.pem cert.pem > mumble.pem
```

Now as root again (`sudo -i`), edit `/etc/systemd/system/mumble.service` appending `-username USERNAME_TO_REGISTER -certificate /home/mumble/mumble.pem` at the end of `ExecStart = /home/mumble/bin/talkiepi`

Run `systemctl daemon-reload` and then `service mumble restart` and tls certificate should be all set!


### Original Source
talkiepi is a headless capable Mumble client written in Go, written for walkie talkie style use on the Pi using GPIO pins for push to talk and LED status.  It is a fork of [barnard](https://github.com/layeh/barnard), which was a great jump off point for me to learn golang and have something that worked relatively quickly.



